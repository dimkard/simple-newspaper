# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-only
import subprocess
from urllib.parse import urlsplit
from pathlib import Path
import logging
import re
import requests
from readability import Document

URL_PATTERN = r"(?i)\b((?:https?:(?:/{1,3}|[a-z0-9%])|[a-z0-9.\-]+[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)/)(?:[^\s()<>{}\[\]]+|\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\))+(?:\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\)|[^\s`!()\[\]{};:'.,<>?])|(?:(?<!@)[a-z0-9]+(?:[.\-][a-z0-9]+)*[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)\b/?(?!@)))"


class HtmlEpub:
    """Manages the generation of epub documents from URLs"""

    def __init__(self, urls_file_path: str, target_dir: str, flatpak: bool):
        self._urls_file_path = urls_file_path
        self._target_dir = target_dir
        self._flatpak = flatpak

    def urls_to_books(self):
        """Given a path to a file that contains a list of URLs, it generates a set
        of epub files"""

        lines = []
        with open(self._urls_file_path, "r", encoding="utf8") as file:
            lines = [line.rstrip() for line in file]

        for line in lines:
            extracted_url = self._url_from_text(line)
            if extracted_url and (line[0] != "x"):
                logging.info("Found %s", extracted_url)
                self._url_to_epub_doc(extracted_url)

    @staticmethod
    def _url_to_filename(url=None) -> str:
        """Constructs a file name from a URL"""
        if url is None:
            return ""
        urlpath = urlsplit(url).path
        return Path(urlpath).stem

    def _html_file_to_epub_doc(self, file_path: str):
        """Converts an html file to an epub document"""
        logging.info("Converting %s to epub", file_path)
        cmd = f"ebook-convert '{file_path}' '{file_path}.epub'"
        if self._flatpak:
            cmd = f"flatpak --command='sh' run com.calibre_ebook.calibre -c \"{cmd}\""

        subprocess.run(cmd, shell=True, check=True)

    def _url_to_epub_doc(self, url):
        """Converts a URL to an epub book. It returns the path to the book file"""

        html_file_path = self._download_url(url)
        if html_file_path:
            self._html_file_to_epub_doc(html_file_path)
            Path(html_file_path).unlink()

    def _download_url(self, article_url: str) -> str:
        """Extracts the first url found in a line of text. If the url is
        valid and downloaded correctly, a path with the content of the file will
        be returned. Otherwise, an empty string is returned."""
        logging.info("Downloading %s to %s", article_url, self._target_dir)
        headers = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0"
        }

        try:
            response = requests.get(article_url, timeout=120, headers=headers)

            if response.status_code == 200:
                doc = Document(response.content)
                file_name = self._url_to_filename(article_url)
                file_path = f"{self._target_dir}/{file_name}.html"

                with open(file_path, "w", encoding="utf8") as file:
                    file.write(doc.summary())

                    logging.info(
                        "URL %s downloaded successfully into %s", article_url, file_path
                    )
        except requests.exceptions.RequestException as err:
            logging.error("URL %s cannot be downloaded. %s", article_url, err)
            file_path = ""

        return file_path

    @staticmethod
    def _url_from_text(line: str) -> str:
        """Returns a URL found in a string. The string should be:
        - A valid URL, e.g. str = "https://example.com/path"
        - A markdown formatted URL, e.g. str = [Title](https://example.com/path)"""

        inline_link_pattern = re.compile(URL_PATTERN)
        inline_links = inline_link_pattern.findall(line)
        if inline_links:
            return inline_links[0]

        return ""
