# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-only
"""Execute operations on disk"""

import shutil
from pathlib import Path
from datetime import datetime
import subprocess


def mount(target_device: str, target_mount_point: str):
    """Mounts the external drive"""
    cmd = f"sudo mount -o umask=0022,gid=1000,uid=1000 {target_device} {target_mount_point}"
    subprocess.run(cmd, shell=True, check=True)


def archive(source_dir_path_name: str, target_dir_path_name: str):
    """ "Archive the old epub files"""
    if Path(source_dir_path_name).is_dir():
        curr_date = datetime.now()
        time_suffix = curr_date.strftime("%Y%m%d%H%M%S")
        shutil.move(
            source_dir_path_name, f"{target_dir_path_name}/backup.{time_suffix}"
        )


def umount(target_device: str):
    """Unmounts the external drive"""
    cmd = f"sudo umount {target_device}"
    subprocess.run(cmd, shell=True, check=True)


def remove_dir(dir_path: Path):
    """Removes the directory provided"""
    if dir_path.is_dir():
        shutil.rmtree(dir_path)


def refresh_dir(dir_path: Path):
    """Clears and recreates the directory provided"""
    remove_dir(dir_path)
    dir_path.mkdir(parents=True, exist_ok=True)
