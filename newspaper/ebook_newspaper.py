# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-only
"""Converts Calibre recipe(s) and a text with URLs to epub documents"""
import sys
import argparse
import logging
from pathlib import Path
import shutil
from rss_epub import RssEpub
from html_epub import HtmlEpub
import storage

STAGE_DIR = "./tmp"
ARTICLES_DIR = "articles"
ARCHIVE_DIR = "news_archive"


def _cmd_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        prog="Newspaper",
        description="Generate an epub newspaper based on calibre recipes and a text with urls",
    )
    parser.add_argument(
        "--recipes_dir",
        help="""Directory that contains the index.json and the Calibre recipes.
            If empty, no RSS will be fetched.""",
        type=str,
    )
    parser.add_argument(
        "--target_device",
        help="""
        The device path of the external storage, e.g. /dev/disk/by-uuid/7afb-3231.
        If empty, the epub documents will be saved to the home_target_dir provided.""",
        type=str,
    )
    parser.add_argument(
        "--target_mount_point",
        help="Path to mount the external storage to, e.g. /mnt/jane/sd",
        type=str,
    )
    parser.add_argument(
        "--target_dir",
        help="""Path to the directory to save the epub files into. It should be
        a relative path on the target device, e.g. newspaper/news""",
        type=str,
    )
    parser.add_argument(
        "--home_target_dir",
        help="""An absolute path on a home directory to save the epub documents.
        To be set when no external storage is needed.""",
        type=str,
    )
    parser.add_argument(
        "--read_it_later_urls",
        help="""Path to the file that contains the list of URLS that should be
        converted to epub files. Leave it empty if not needed.""",
        type=str,
    )
    parser.add_argument(
        "--archive",
        help="Archive the old articles",
        action="store_true",
    )
    parser.add_argument(
        "--flatpak",
        help="Calibre is installed as Flatpak",
        action="store_true",
    )

    return parser.parse_args()


def _input_validation(arguments: argparse.Namespace):
    if not arguments.recipes_dir and not arguments.read_it_later_urls:
        logging.warning("Nothing to execute")
        sys.exit()

    if not arguments.target_dir and not arguments.home_target_dir:
        logging.error("Please provide a target directory")

    if (not arguments.target_mount_point and arguments.target_device) or (
        arguments.target_mount_point and not arguments.target_device
    ):
        logging.error("Please provide a target device and mount point")


def main():
    """Executes the program"""

    logging.basicConfig(level="INFO")
    args = _cmd_args()
    _input_validation(args)
    if args.target_device:
        storage.mount(args.target_device, args.target_mount_point)

    target_dir = (
        f"{args.target_mount_point}/{args.target_dir}"
        if args.target_device
        else args.home_target_dir
    )
    archive_dir = (
        f"{args.target_mount_point}/{args.target_dir}/../{ARCHIVE_DIR}"
        if args.target_device
        else f"{args.home_target_dir}/../{ARCHIVE_DIR}"
    )

    try:
        if args.archive:
            storage.archive(target_dir, archive_dir)
        else:
            storage.remove_dir(Path(target_dir))

        storage.refresh_dir(Path(STAGE_DIR))

        if args.recipes_dir:
            rss_epub = RssEpub(args.recipes_dir, STAGE_DIR, args.flatpak)
            rss_epub.rss_recipes_to_epub()

        if args.read_it_later_urls:
            Path(f"{STAGE_DIR}/{ARTICLES_DIR}").mkdir(exist_ok=True)
            html_epub = HtmlEpub(
                args.read_it_later_urls, f"{STAGE_DIR}/{ARTICLES_DIR}", args.flatpak
            )
            html_epub.urls_to_books()

        shutil.move(STAGE_DIR, target_dir)
    except Exception:
        if args.target_device:
            storage.umount(args.target_device)
        raise

    if args.target_device:
        storage.umount(args.target_device)


if __name__ == "__main__":
    main()
