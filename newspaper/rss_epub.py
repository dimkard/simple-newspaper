# SPDX-FileCopyrightText: 2023 Dimitris Kardarakos <dimkard@posteo.net>
# SPDX-License-Identifier: GPL-3.0-only
import json
import subprocess


class RssEpub:
    """Manages the generation of epub documents from RSS"""

    def __init__(self, recipes_dir: str, target_dir: str, flatpak: bool):
        self._recipes_dir = recipes_dir
        self._flatpak = flatpak
        self._target_dir = target_dir

    def _calibre_recipes(self) -> dict:
        """Returns a dictionary with the Calibre recipes"""
        recipes_cfg_path = f"{self._recipes_dir}/index.json"

        with open(recipes_cfg_path, "r", encoding="utf-8") as recipes_cfg:
            recipes = json.load(recipes_cfg)

        return recipes

    def rss_recipes_to_epub(self):
        """Download and convert to ebooks the Calibre recipes found in the
        directory provided"""
        recipes = self._calibre_recipes()

        for idx in recipes:
            fetch_cmd = f"ebook-convert '{self._recipes_dir}/{recipes[idx][1]}' '{self._target_dir}/{recipes[idx][0]}.epub'"

            if self._flatpak:
                fetch_cmd = f"flatpak --command='sh' run com.calibre_ebook.calibre -c \"{fetch_cmd}\""

            subprocess.run(fetch_cmd, shell=True, check=True)
