format:
	python3 -m black newspaper

typehint:
	python3 -m mypy newspaper

lint:
	python3 -m pylint --rcfile=./pylintrc newspaper

reuse:
	python3 -m reuse lint 

all:
	make -i format
	make -i lint
	make -i typehint
	make -i reuse
.PHONY: format typehint test lint checklist

