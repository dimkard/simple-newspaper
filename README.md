# Simple newspaper

Terminal utility that generates EPUB documents from RSS feeds and URLs.

## Dependencies

- [Calibre](https://calibre-ebook.com/) e-book management application.
-  Python modules; run `pip install -r requirements.txt` to install them

## Configure

### RSS
RSS feeds should be added to [Calibre recipes](https://manual.calibre-ebook.com/news.html). Calibre recipes' configuration files can be found at `~/.config/calibre/custom_recipes` or, if Calibre has been installed as flatpak, `~/.var/app/com.calibre_ebook.calibre/config/calibre/custom_recipes`.

### URLs
The links that should be converted to EPUB documents should be added to a plain text file, e.g. `~/Documents/read-it-later.txt`, like this:

```
www.example.com/my_article.html
Another article www.example.com/another_article
Yet another [article](www.example.com/yet_another.html)
```

### Execute

The simple-newspaper utility saves the EPUB documents either into an external, not mounted storage device or into a normal directory in your file system.

#### External storage device

For the EPUB documents to be saved into a not mounted device, the following are needed:

- the path to the external device, e.g. `/dev/disk/by-uuid/7afb-3231`
- the directory that the device should be mounted to, e.g. `/mnt/jane/sd`
- the directory to save the EPUB files into. It should be a relative path on the target device, e.g. `newspaper/news`

Run:

```
python ebook_newspaper.py --recipes_dir ~/.config/calibre/custom_recipes --target_device /dev/disk/by-uuid/7afb-3231 --target_mount_point /mnt/jane/sd --target_dir newspaper/news --read_it_later_urls ~/Documents/read-it-later.txt
```

The utility will ask for the sudo password for the external storage to be mounted.

#### File system directory
For the EPUB documents to be saved into a directory in your `$HOME`, e.g. `~/Documents/newspaper`, execute:

```
python ebook_newspaper.py --recipes_dir ~/.config/calibre/custom_recipes --home_target_dir ~/Documents/newspaper --read_it_later_urls ~/Documents/read-it-later.txt
```

##### More options
You can archive the old articles by setting the `--archive` option. If Calibre has been installed as flatpak, add the `--flatpak` option. E.g.,

```
python ebook_newspaper.py --recipes_dir ~/.var/app/com.calibre_ebook.calibre/config/calibre/custom_recipes --target_device /dev/disk/by-uuid/7afb-3231 --target_mount_point /mnt/jane/sd --target_dir newspaper/news --read_it_later_urls ~/Documents/read-it-later.txt --archive --flatpak
```
